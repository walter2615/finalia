import cv2
import sys

# Obtener valores proporcionados por el usuario
imagePath = sys.argv[1]
cascPath = "frontal.xml"

# Crea la cascada de haar
faceCascade = cv2.CascadeClassifier(cascPath)

# Lee la imagen
image = cv2.imread(imagePath)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# Detectar caras en la imagen
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30),
    flags = cv2.CASCADE_SCALE_IMAGE
)

print("Found {0} faces!".format(len(faces)))

# Dibuja un rectangulo alrededor de las caras
for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x+w, y+h), (0, 255, 0), 2)

cv2.imshow("Faces found", image)
cv2.waitKey(0)
